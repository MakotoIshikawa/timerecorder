﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AmsDatabaseConnectionLibrary;
using ExtensionsLibrary.Extensions;
using JsonLibrary.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeRecorder.Enums;

namespace TimeRecorder.Helper.Tests {
	[TestClass()]
	public class WebApiHelperTests {
		#region フィールド

		private string _site = @"http://vmypc2016/AMS_API";

		#endregion

		#region メソッド

		[TestMethod()]
		[Owner(nameof(WebApiHelper))]
		[TestCategory("取得")]
		public async Task CheckUserHasAdministratorAuthorityAsyncTest() {
			var wah = new WebApiHelper(this._site);
			{// 正常系
				var card = "0000000000000011";

				var hasAdmin = await wah.CheckUserHasAdministratorAuthorityAsync(card);

				// 1: Trueで返ってくること
				Assert.IsTrue(hasAdmin);
			}
			{//異常系:必要な権限レベルを満たしていない
				var card = "0000000000000001";

				var hasAdmin = await wah.CheckUserHasAdministratorAuthorityAsync(card);

				// 2: Falseで返ってくること
				Assert.IsFalse(hasAdmin);
			}
		}

		[TestMethod()]
		[Owner(nameof(WebApiHelper))]
		[TestCategory("取得")]
		public async Task GetUsersAsyncTest() {
			var wah = new WebApiHelper(this._site);
			var users = await wah.GetUsersAsync();

			// 3: Trueで返ってくること
			Assert.IsTrue(users.Any());
		}

		[TestMethod()]
		[Owner(nameof(WebApiHelper))]
		[TestCategory("取得")]
		public async Task GetUsersAsyncTest1() {
			var wah = new WebApiHelper(this._site);
			{// 正常系
				var orgId = 3;

				var users = await wah.GetUsersAsync(orgId);
				var ls = users.ToList();

				// 4: 取得したユーザー件数が4件であること
				Assert.AreEqual(4, users.Count());
				// 5: 1件目のユーザ名が「真田昇一」であること
				Assert.AreEqual("真田昇一", ls[0].user_name);
				// 6: 2件目のユーザ名が「福地彰英」であること
				Assert.AreEqual("福地彰英", ls[1].user_name);
				// 7: 3件目のユーザ名が「中谷達」であること
				Assert.AreEqual("中谷達", ls[2].user_name);
				// 8: 4件目のユーザ名が「岡田和義」であること
				Assert.AreEqual("岡田和義", ls[3].user_name);
			}
			{//異常系:店舗IDに所属するユーザーがいなかった場合
				var orgId = 99;

				var users = await wah.GetUsersAsync(orgId);

				// 9: ユーザー情報が取得できないこと
				Assert.IsFalse(users.Any());
			}
		}

		[TestMethod()]
		[Owner(nameof(WebApiHelper))]
		[TestCategory("更新")]
		public async Task PutCardNumTest() {
			var wah = new WebApiHelper(this._site);
			{// 正常系
				var id = "0000000011";
				var cardNum = "2222222222222222";

				var response = await wah.PutCardNumAsync(id, cardNum);

				// 10: HTTPステータスコードがOK(200)で返ってくること
				Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

				var contents = await response.Content.ReadAsStringAsync();
				var user = contents.Deserialize<m_user>();

				// 11: 取得したユーザー名が「五十嵐南」であること
				Assert.AreEqual("五十嵐南", user.user_name);
			}
			{// 異常系：存在しないユーザーIDでカード番号を更新
				var id = "BBBBBBBBBB";
				var cardNum = "BBBBBBBBBBBBBBBB";

				var response = await wah.PutCardNumAsync(id, cardNum);

				// 12: HTTPステータスコードがNotFound(404)で返ってくること
				Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);

			}
			{// 異常系：登録しているカード番号と同じ値でカード番号を更新
				var id = "0000000011";
				var cardNum = "2222222222222222";

				var response = await wah.PutCardNumAsync(id, cardNum);

				// 13: HTTPステータスコードがNotModified(304)で返ってくること
				Assert.AreEqual(HttpStatusCode.NotModified, response.StatusCode);
			}
		}

		[TestMethod()]
		[Owner(nameof(WebApiHelper))]
		[TestCategory("登録")]
		public async Task RegistAsyncTest() {
			var wah = new WebApiHelper(this._site);
			{//正常系：出勤登録
				var workCategory = WorkCategoryTypes.出勤;
				var card = "0000000000000001";

				var response = await wah.RegistAsync(workCategory, card);

				// 14: HTTPステータスコードがOK(200)で返ってくること
				Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

				var contents = await response.Content.ReadAsStringAsync();
				var attendance = contents.Deserialize<ATTENDANCE>();

				var today = DateTime.Today;

				// 15: 現在の日付と登録した出勤時刻の日付が一致すること
				Assert.AreEqual(today, attendance.ENTRY_TIME.Date);
			}
			{// 異常系:出勤時刻に紐づく退勤時刻が登録されていない
				var workCategory = WorkCategoryTypes.出勤;
				var card = "0000000000000001";

				var response = await wah.RegistAsync(workCategory, card);

				// 16: HTTPステータスコードがForbidden(403)で返ってくること
				Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
			}
			{// 異常系：ユーザー情報に存在しないカード番号で出勤時刻を登録
				var card = "BBBBBBBBBBBBBBBB";
				var workCategory = WorkCategoryTypes.出勤;

				var response = await wah.RegistAsync(workCategory, card);

				// 17: HTTPステータスコードがBadRequest(400)で返ってくること
				Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
			}
			{//正常系：退勤登録
				var workCategory = WorkCategoryTypes.退勤;
				var card = "0000000000000001";

				var response = await wah.RegistAsync(workCategory, card);

				// 18: HTTPステータスコードがOK(200)で返ってくること
				Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

				var contents = await response.Content.ReadAsStringAsync();
				var attendance = contents.Deserialize<ATTENDANCE>();

				// 19: 退勤時刻に値が入っていること
				Assert.IsTrue(attendance.EXIT_TIME.HasValue);
			}
			{// 異常系:出勤時刻に紐づく退勤時刻が登録されている
				var workCategory = WorkCategoryTypes.退勤;
				var card = "0000000000000001";

				var response = await wah.RegistAsync(workCategory, card);

				// 20: HTTPステータスコードがNotFound(404)で返ってくること
				Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
			}
			{// 異常系：ユーザー情報に存在しないカード番号で退勤時刻を登録
				var workCategory = WorkCategoryTypes.退勤;
				var card = "BBBBBBBBBBBBBBBB";

				var response = await wah.RegistAsync(workCategory, card);

				// 21: HTTPステータスコードがBadRequest(400)で返ってくること
				Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
			}
		}

		[TestMethod()]
		[Owner(nameof(WebApiHelper))]
		[TestCategory("登録")]
		public async Task RegistWorkAsyncTest() {
			{// 正常系
				var id = "0000000005";
				var entry = new DateTime(2018, 6, 20, 8, 50, 0);
				var exit = new DateTime(2018, 6, 20, 17, 50, 0);
				var wah = new WebApiHelper(this._site);

				var response = await wah.RegistWorkAsync(id, entry, exit);

				// 22: HTTPステータスコードがOk(200)で返ってくること
				Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
			}
			{// 異常系：ユーザー情報に存在しないユーザーIDで日次勤怠情報を登録
				var id = "BBBBBBBBBB";
				var entry = new DateTime(2018, 6, 20, 8, 50, 0);
				var exit = new DateTime(2018, 6, 20, 17, 50, 0);
				var wah = new WebApiHelper(this._site);

				var response = await wah.RegistWorkAsync(id, entry, exit);

				// 23: HTTPステータスコードがNotFound(404)で返ってくること
				Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
			}
			{// 異常系：登録している出勤時刻と退勤時刻で入退室時刻を更新
				var id = "0000000005";
				var entry = new DateTime(2018, 6, 20, 8, 50, 0);
				var exit = new DateTime(2018, 6, 20, 17, 50, 0);
				var wah = new WebApiHelper(this._site);

				var response = await wah.RegistWorkAsync(id, entry, exit);

				// 24: HTTPステータスコードがNotModified(304)で返ってくること
				Assert.AreEqual(HttpStatusCode.NotModified, response.StatusCode);
			}
		}

		[TestMethod()]
		[Owner(nameof(WebApiHelper))]
		[TestCategory("登録")]
		public async Task RegistWorkAsyncTest2() {
			{// 正常系
				var id = "019165";
				var entry = new DateTime(2018, 8, 3, 22, 00, 0);
				var exit = new DateTime(2018, 8, 4, 05, 30, 0);
				var wah = new WebApiHelper(this._site);

				var response = await wah.RegistWorkAsync(id, entry, exit);

				// 22: HTTPステータスコードがOk(200)で返ってくること
				Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
			}
		}

		#endregion
	}
}