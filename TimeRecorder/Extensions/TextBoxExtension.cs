﻿using System.Windows.Controls;

namespace TimeRecorder.Extensions {
	/// <summary>
	/// TextBox を拡張するメソッドを提供するクラスです。
	/// </summary>
	public static partial class TextBoxExtension {
		#region メソッド

		/// <summary>
		/// テキストボックスをリセットします。
		/// </summary>
		/// <param name="this">TextBox</param>
		public static void Reset(this TextBox @this) {
			@this?.Clear();
			@this?.Focus();
		}

		#endregion
	}
}
