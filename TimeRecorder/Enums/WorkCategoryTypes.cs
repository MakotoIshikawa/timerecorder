﻿namespace TimeRecorder.Enums {
	/// <summary>
	/// 勤怠種別の列挙体です。
	/// </summary>
	public enum WorkCategoryTypes {
		/// <summary>
		/// 出勤
		/// </summary>
		出勤,

		/// <summary>
		/// 退勤
		/// </summary>
		退勤,
	}
}
