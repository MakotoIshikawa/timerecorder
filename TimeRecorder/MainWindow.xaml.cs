﻿using System;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using AmsDatabaseConnectionLibrary;
using CommonFeaturesLibrary;
using JsonLibrary.Extensions;
using TimeRecorder.Data;
using TimeRecorder.Dialogues;
using TimeRecorder.Enums;
using TimeRecorder.Extensions;
using TimeRecorder.Helper;
using TimeRecorder.Properties;
using WpfLibrary.Extensions;

namespace TimeRecorder {
	/// <summary>
	/// MainWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class MainWindow : Window {
		#region 定数

		/// <summary>
		/// システムコマンド
		/// </summary>
		public const int WM_SYSCOMMAND = 0x0112;

		/// <summary>
		/// クローズ
		/// </summary>
		public const int SC_CLOSE = 0xF060;

		#endregion

		#region フィールド

		/// <summary>
		/// ログ
		/// </summary>
		private Logger _log = new Logger();

		#endregion

		#region コンストラクタ

		/// <summary>
		/// コンストラクタ
		/// </summary>
		public MainWindow() {
			this.InitializeComponent();
#if DEBUG
			this.WindowStyle = WindowStyle.SingleBorderWindow;
			this.Topmost = false;
			this.txtCardId.SetTop(0.0);
#endif
			try {
				this.radioEntry.Tag = WorkCategoryTypes.出勤;
				this.radioExit.Tag = WorkCategoryTypes.退勤;

				// ロードイベント
				this.windowTop.Loaded += (sender, e)
					=> this.txtCardId.Reset();

				// カード読み取りイベント
				this.CardRead += this.window_CardReadAsync;

				// タイマースレッド起動
				this.StartTimer();

				this.imgGear.Source = Properties.Resources.Gear.GetImageSource(ImageFormat.Png);
				this.imgEntry.Source = Properties.Resources.Entry.GetImageSource(ImageFormat.Png);
				this.imgExit.Source = Properties.Resources.Exit.GetImageSource(ImageFormat.Png);
			} catch (ApplicationException ex) {
				this.ShowWarning(ex);
				Application.Current.Shutdown();
			} catch (FileNotFoundException ex) {
				this.ShowWarning(ex);
				Application.Current.Shutdown();
			} catch (Exception ex) {
				this.ShowError(ex);
				Application.Current.Shutdown();
			}
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// 選択されている勤怠区分を取得します。
		/// </summary>
		public WorkCategoryTypes SelectWorkCategory
			=> (WorkCategoryTypes)this.GetDescendants<RadioButton>().FirstOrDefault(x => x.IsChecked ?? false)?.Tag;

		#endregion

		#region イベント

		/// <summary>
		/// カード読み取りイベント
		/// </summary>
		public event EventHandler<CardReadEventArgs> CardRead;

		/// <summary>
		/// カード読み取りイベント
		/// </summary>
		protected virtual void OnCardRead(string card)
			=> this.CardRead?.Invoke(this, new CardReadEventArgs(card));

		#endregion

		#region イベントハンドラ

		/// <summary>
		/// カードが読み込まれた後に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private async void window_CardReadAsync(object sender, CardReadEventArgs e) {
			try {
				var work = this.SelectWorkCategory;
				var data = await RegistAttendanceAsync(work, e.CardNumber);
				if (!data.Success) {
					// 警告音を鳴らす
					SystemSounds.Hand.Play();
				}

				this.ShowMessage($"{data}");

				if (data.Success && work == WorkCategoryTypes.退勤) {
					await RegistWorkAsync(data);
				}
			} catch (ApplicationException ex) {
				this.ShowWarning(ex);
			} catch (Exception ex) {
				this.ShowError(ex);
			}
		}

		/// <summary>
		/// テキストボックの値が変更された時に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private void textBox_TextChanged(object sender, TextChangedEventArgs e) {
			var txt = sender as TextBox;
			try {
				var card = txt?.Text;

				if (card.Length < Settings.Default.DigitsCount) {
					return;
				}

				txt?.Reset();

				this.OnCardRead(card);
			} catch (Exception ex) {
				txt?.Reset();
				this.ShowError(ex);
			}
		}

		/// <summary>
		/// ラジオボタンが選択された時に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private void radio_Checked(object sender, RoutedEventArgs e) {
			this.txtCardId.Reset();
		}

		/// <summary>
		/// 歯車アイコンを表示しているボタンをクリックした時に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private void btnMenu_Click(object sender, RoutedEventArgs e) {
			this.menuFunction.IsSubmenuOpen = true;
			this.txtCardId.Reset();
		}

		/// <summary>
		/// メニューから勤怠登録が押下された時に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベント</param>
		private void menuAttendance_Click(object sender, RoutedEventArgs e) {
			try {
				var dlg = new AttendanceRegisterDialog { Owner = this, WindowStartupLocation = WindowStartupLocation.CenterOwner };
				dlg.ShowDialog();
				switch (dlg.Result) {
				case MessageBoxResult.None:
					this.ShowMessage("権限レベルを満たしていません。");
					break;
				case MessageBoxResult.OK:
					this.OnCardRead(dlg.CardNum);
					break;
				case MessageBoxResult.Cancel:
					this.ShowMessage("代理登録をキャンセルしました。");
					break;
				}
			} catch (Exception ex) {
				this.ShowError(ex);
			} finally {
				this.txtCardId.Reset();
			}
		}

		/// <summary>
		/// メニューからカード登録が押下された時に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private void menuCard_Click(object sender, RoutedEventArgs e) {
			try {
				var dlg = new CardRegisterDialog { Owner = this, WindowStartupLocation = WindowStartupLocation.CenterOwner };
				dlg.ShowDialog();
				switch (dlg.Result) {
				case MessageBoxResult.None:
					this.ShowMessage("権限レベルを満たしていません。");
					break;
				case MessageBoxResult.OK:
					this.ShowMessage("新しいカード登録しました。");
					break;
				case MessageBoxResult.Cancel:
					this.ShowMessage("カード登録を中止ました。");
					break;
				}
			} catch (Exception ex) {
				this.ShowError(ex);
			} finally {
				this.txtCardId.Reset();
			}
		}

		/// <summary>
		/// マウス キャプチャを失った際に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private void control_LostMouseCapture(object sender, RoutedEventArgs e) {
			this.txtCardId.Reset();
		}

		/// <summary>
		/// フォーカスされた際に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private void textBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e) {
			CloseOnscreenKeyboard();
		}

		#endregion

		#region メソッド

		/// <summary>
		/// タイマースレッド起動
		/// </summary>
		private void StartTimer() {
			var timer = new DispatcherTimer() {
				Interval = new TimeSpan(0, 0, 1)
			};

			timer.Tick += (sender, e) => {
				var format = "yyyy年M月d日　HH:mm";
#if DEBUG
				format += ":ss";
#endif
				this.lblTime.Content = DateTime.Now.ToString(format);
			};

			timer.Start();
		}

		#region 出退勤登録

		private async Task<RegistData> RegistAttendanceAsync(WorkCategoryTypes workCategory, string cardId) {
			var wah = new WebApiHelper(Settings.Default.ApiPath);
			var response = await wah.RegistAsync(workCategory, cardId);
			switch (response.StatusCode) {
			case HttpStatusCode.OK:
				var contents = await response.Content.ReadAsStringAsync();
				var req = contents.Deserialize<ATTENDANCE>();
				var data = new RegistData() {
					Success = true,
					UserId = req.USER_ID,
					RegisterName = req.USER_NAME,
					EntryTime = req.ENTRY_TIME,
					ExitTime = req.EXIT_TIME,
					WorkCategory = workCategory,
				};

				return data;
			case HttpStatusCode.Forbidden:
				return new RegistData() { Success = false, WorkCategory = WorkCategoryTypes.出勤, };
			case HttpStatusCode.NotFound:
				return new RegistData() { Success = false, WorkCategory = WorkCategoryTypes.退勤, };
			case HttpStatusCode.BadRequest:
				var token = await response.Content.ToJTokenAsync();
				throw new ApplicationException($"{token["Message"]}");
			case HttpStatusCode.InternalServerError:
				throw new ApplicationException("サーバーエラーが発生しました。");
			default:
				throw new Exception($"想定外の例外が発生しました。StatusCode={response.StatusCode}");
			}
		}

		#endregion

		#region 日次情報登録

		/// <summary>
		/// [非同期]
		/// 日次勤怠情報に出退勤情報を登録します。
		/// </summary>
		/// <param name="data">登録データ</param>
		private static async Task RegistWorkAsync(RegistData data) {
			var wah = new WebApiHelper(Settings.Default.ApiPath);
			var response = await wah.RegistWorkAsync(data.UserId, data.EntryTime, data.ExitTime.Value);
			if (response.StatusCode != HttpStatusCode.OK) {
				throw new ApplicationException("日次情報の登録に失敗しました。");
			}
		}

		#endregion

		#region メッセージ表示

		/// <summary>
		/// メッセージを表示します。
		/// </summary>
		/// <param name="message">メッセージ</param>
		protected virtual void ShowMessage(string message) {
			this.lblMessage.Content = message;
			this._log.WriteLog($"[{nameof(MessageBoxImage.Information)}] {message}");
		}

		/// <summary>
		/// 警告レベルの例外を表示します。
		/// </summary>
		/// <param name="ex">例外</param>
		protected virtual void ShowWarning(Exception ex) {
			this.ShowMessageBox(ex.Message, nameof(MessageBoxImage.Warning), icon: MessageBoxImage.Warning);
			this._log.WriteLog($"[{nameof(MessageBoxImage.Warning)}] {ex.Message}");
		}

		/// <summary>
		/// エラーレベルの例外を表示します。
		/// </summary>
		/// <param name="ex">例外</param>
		protected virtual void ShowError(Exception ex) {
			this.ShowMessageBox(ex.ToString(), nameof(MessageBoxImage.Error), icon: MessageBoxImage.Error);
			this._log.WriteLog($"[{nameof(MessageBoxImage.Error)}] {ex}");
		}

		/// <summary>
		/// 指定された文字列と一致するクラス名とウィンドウ名を持つトップレベルウィンドウ（親を持たないウィンドウ）のハンドルを返します。
		/// この関数は、子ウィンドウは探しません。検索では、大文字小文字は区別されません。
		/// </summary>
		/// <param name="lpClassName">クラス名</param>
		/// <param name="lpWindowName">ウィンドウ名</param>
		/// <returns>
		/// 関数が成功すると、指定したクラス名とウィンドウ名を持つウィンドウのハンドルが返ります。
		/// 関数が失敗すると、NULL が返ります。拡張エラー情報を取得するには、 関数を使います。
		/// </returns>
		[DllImport("user32.dll")]
		public static extern int FindWindow(string lpClassName, string lpWindowName);

		/// <summary>
		/// 1 つまたは複数のウィンドウへ、指定されたメッセージを送信します。
		/// この関数は、指定されたウィンドウのウィンドウプロシージャを呼び出し、
		/// そのウィンドウプロシージャがメッセージを処理し終わった後で、制御を返します。
		/// </summary>
		/// <param name="hWnd">送信先ウィンドウのハンドル</param>
		/// <param name="Msg">メッセージ</param>
		/// <param name="wParam">メッセージの最初のパラメータ</param>
		/// <param name="lParam">メッセージの 2 番目のパラメータ</param>
		/// <returns>メッセージ処理の結果が返ります。この戻り値の意味は、送信されたメッセージにより異なります。</returns>
		[DllImport("user32.dll")]
		public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);

		/// <summary>
		/// スクリーンキーボードを閉じます。
		/// </summary>
		public static void CloseOnscreenKeyboard() {
			// ウィンドウのハンドラを取得する
			var iHandle = FindWindow("IPTIP_Main_Window", "");
			if (iHandle > 0) {
				// APIを使用してウィンドウを閉じる
				SendMessage(iHandle, WM_SYSCOMMAND, SC_CLOSE, 0);
			}
		}

		#endregion

		#endregion
	}
}
