﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AmsDatabaseConnectionLibrary;
using ExtensionsLibrary.Extensions;
using TimeRecorder.Data;
using TimeRecorder.Extensions;
using TimeRecorder.Helper;
using TimeRecorder.Interface;
using TimeRecorder.Properties;
using WpfLibrary.Extensions;

namespace TimeRecorder.Dialogues.Primitives {
	/// <summary>
	/// SettingDialogBase.xaml の相互作用ロジック
	/// </summary>
	public abstract partial class SettingDialogBase : Window, ISettingDialog {
		#region コンストラクタ

		/// <summary>
		/// コンストラクタ
		/// </summary>
		protected SettingDialogBase() {
			this.InitializeComponent();

			this.cmbUsers.BindData(this.ViewUsers, u => u.user_name, u => u.card_id);
			this.cmbOrgs.BindData(this.ViewOrgs, o => o.alias, o => o.org_id);

			this.txtCardId.Reset();

			// カード読み取りイベント
			this.CardRead += this.dialog_CardReadAsync;
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// ダイアログの応答
		/// </summary>
		public MessageBoxResult Result { get; protected set; } = MessageBoxResult.Cancel;

		/// <summary>
		/// 現在選択されているカード番号を取得します。
		/// </summary>
		public string CardNum
			=> this.cmbUsers.SelectedValue?.ToString();

		/// <summary>
		/// 現在選択されている組織のIDを取得します。
		/// </summary>
		public int SelectedOrgId
			=> (int)this.cmbOrgs.SelectedValue;

		/// <summary>
		/// ユーザー選択にバインドする表示用のコレクションを取得、設定します。
		/// </summary>
		protected virtual ObservableCollection<m_user> ViewUsers { get; set; } = new ObservableCollection<m_user>();

		/// <summary>
		/// ユーザー選択にバインドする表示用のコレクションを取得、設定します。
		/// </summary>
		protected virtual ObservableCollection<m_org> ViewOrgs { get; set; } = new ObservableCollection<m_org>();

		/// <summary>
		/// 画面が編集可能かどうかを取得、設定します。
		/// </summary>
		public virtual bool Editable {
			get {
				return this.cmbOrgs.IsEnabled
					&& this.cmbUsers.IsEnabled;
			}
			set {
				this.cmbOrgs.IsEnabled = value;
				this.cmbUsers.IsEnabled = value;
			}
		}

		#endregion

		#region イベント

		/// <summary>
		/// カード読み取りイベント
		/// </summary>
		public event EventHandler<CardReadEventArgs> CardRead;

		/// <summary>
		/// カード読み取りイベント
		/// </summary>
		protected virtual void OnCardRead(string cardNum)
			=> this.CardRead?.Invoke(this, new CardReadEventArgs(cardNum));

		#endregion

		#region イベントハンドラ

		/// <summary>
		/// ウインドウが読み込まれた後に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private async void Window_Loaded(object sender, RoutedEventArgs e) {
			var wah = new WebApiHelper(Settings.Default.ApiPath);
			var orgs = await wah.GetOrgsAsync();

			this.ViewOrgs.AddRange(orgs.Where(o => !o.alias.IsWhiteSpace()));
			this.cmbOrgs.SelectedIndex = 0;
		}

		/// <summary>
		/// カードが読み込まれた後に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private async void dialog_CardReadAsync(object sender, CardReadEventArgs e) {
			if (this.Editable) {
				return;
			}

			this.txtCardId.IsEnabled = false;

			var cardId = e.CardNumber;
			var wah = new WebApiHelper(Settings.Default.ApiPath);
			var hasAdmin = await wah.CheckUserHasAdministratorAuthorityAsync(cardId);
			if (!hasAdmin) {
				this.Result = MessageBoxResult.None;
				this.Close();
			}

			this.Editable = true;

			var msg = "登録者が所属する店舗を選択し、名前を選択してください。";
			this.SetMessage(msg);
		}

		/// <summary>
		/// テキストボックの値が変更された時に呼ばれる
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private void txtCardId_TextChanged(object sender, TextChangedEventArgs e) {
			var txt = sender as TextBox;
			try {
				var cardId = txt?.Text;

				if (cardId.Length < Settings.Default.DigitsCount) {
					return;
				}

				txt.Reset();

				this.OnCardRead(cardId);
			} catch (Exception ex) {
				txt.Reset();
				this.ShowMessageBox(ex.Message, icon: MessageBoxImage.Error);
			}
		}

		/// <summary>
		/// コンボボックスの項目が選択された時に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private async void cmbOrgs_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			var belongId = this.SelectedOrgId;

			var wah = new WebApiHelper(Settings.Default.ApiPath);
			var us = await wah.GetUsersAsync(belongId);

			this.ViewUsers.Clear();
			this.ViewUsers.AddRange(us);

			this.btnOk.IsEnabled = false;
			var msg = this.Editable
				? "登録者の名前を選択してください。"
				: "設定権限を持つユーザーのカードを読取らせて下さい。";

			this.SetMessage(msg);
		}

		/// <summary>
		/// コンボボックスの項目が選択された時に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		protected virtual void cmbUsers_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			this.btnOk.IsEnabled = true;
			this.SetMessage("OKボタンを押して下さい。");
		}

		/// <summary>
		/// OKボタンが押下された時に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private void btnOk_Click(object sender, RoutedEventArgs e) {
			this.DialogResult = true;
			this.Result = MessageBoxResult.OK;
		}

		/// <summary>
		/// キャンセルボタンが押下された時に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private void btnCancel_Click(object sender, RoutedEventArgs e) {
			this.DialogResult = false;
			this.Result = MessageBoxResult.Cancel;
		}

		/// <summary>
		/// マウス キャプチャを失った際に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		private void control_LostMouseCapture(object sender, RoutedEventArgs e) {
			this.txtCardId.Reset();
		}

		#endregion

		#region メソッド

		/// <summary>
		/// 画面に表示するメッセージを設定します。
		/// </summary>
		/// <param name="msg">メッセージ</param>
		protected void SetMessage(string msg)
			=> this.lblMessage.Content = msg;

		#endregion
	}
}
