﻿using System.Windows;
using System.Windows.Controls;
using TimeRecorder.Dialogues.Primitives;
using TimeRecorder.Extensions;
using TimeRecorder.Helper;
using TimeRecorder.Properties;
using WpfLibrary.Extensions;

namespace TimeRecorder.Dialogues {
	/// <summary>
	/// CardRegisterDialog.xaml の相互作用ロジック
	/// </summary>
	public class CardRegisterDialog : SettingDialogBase {
		#region コンストラクタ

		/// <summary>
		/// コンストラクタ
		/// </summary>
		public CardRegisterDialog() : base() {
			this.Initialize();

			this.cmbUsers.BindData(this.ViewUsers, u => u.user_name, u => u.syain_no);
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// 現在選択されているユーザーIDを取得します。
		/// </summary>
		public string UserId
			=> this.cmbUsers.SelectedValue?.ToString();

		#endregion

		#region イベントハンドラ

		/// <summary>
		/// コンボボックスの項目が選択された時に呼ばれます。
		/// </summary>
		/// <param name="sender">送信元</param>
		/// <param name="e">イベントデータ</param>
		protected override void cmbUsers_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			this.btnOk.IsEnabled = true;
			this.txtCardId.IsEnabled = true;
			this.SetMessage("新しく登録するカードを読み込んで下さい。");
			this.txtCardId.Reset();
		}

		#endregion

		#region メソッド

		/// <summary>
		/// 初期化処理
		/// </summary>
		private void Initialize() {
			this.Title = "カード登録";

			// ボタン設定
			this.btnOk.Visibility = Visibility.Collapsed;
			this.btnCancel.Content = "閉じる";

			// カード読み取りイベント
			this.CardRead += async (sender, e) => {
				if (!this.Editable) {
					return;
				}

				var cardId = e.CardNumber;
				var wah = new WebApiHelper(Settings.Default.ApiPath);
				var response = await wah.PutCardNumAsync(this.UserId, cardId);
				if (response.StatusCode == System.Net.HttpStatusCode.OK) {
					this.SetMessage("登録が完了しました。");
				} else {
					this.SetMessage("登録に失敗しました。");
				}
			};
		}

		#endregion
	}
}
