﻿using TimeRecorder.Dialogues.Primitives;

namespace TimeRecorder.Dialogues {
	/// <summary>
	/// AttendanceRegister.xaml の相互作用ロジック
	/// </summary>
	public class AttendanceRegisterDialog : SettingDialogBase {
		#region コンストラクタ

		/// <summary>
		/// コンストラクタ
		/// </summary>
		public AttendanceRegisterDialog() : base() {
			this.Initialize();
		}

		#endregion

		#region プロパティ

		#endregion

		#region イベントハンドラ

		#endregion

		#region メソッド

		/// <summary>
		/// 初期化処理
		/// </summary>
		private void Initialize() {
			this.Title = "勤怠登録";
		}

		#endregion
	}
}
