﻿using System;

namespace TimeRecorder.Interface {
	/// <summary>
	/// 出退勤登録Api実装時に使用するインターフェースです。
	/// </summary>
	interface IRegistData {
		/// <summary>
		/// 出退勤登録の成否
		/// </summary>
		bool Success { get; }

		/// <summary>
		/// ユーザーID
		/// </summary>
		string UserId { get; }

		/// <summary>
		/// 出退勤登録を行ったユーザ名
		/// </summary>
		string RegisterName { get; }

		/// <summary>
		/// 出退勤登録を行った日付時刻
		/// </summary>
		DateTime RegisterTime { get; }
	}
}
