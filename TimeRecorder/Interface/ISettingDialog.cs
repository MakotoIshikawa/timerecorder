﻿using System.Collections.Generic;
using System.Windows;
using TimeRecorder.Enums;

namespace TimeRecorder.Interface {
	/// <summary>
	/// ダイアログ実施時に使用するインターフェースです。
	/// </summary>
	public interface ISettingDialog {
		/// <summary>
		/// ダイアログの応答結果
		/// </summary>
		MessageBoxResult Result { get; }
	}
}