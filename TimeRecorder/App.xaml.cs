﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CommonFeaturesLibrary;

namespace TimeRecorder {
	/// <summary>
	/// App.xaml の相互作用ロジック
	/// </summary>
	public partial class App : Application {
		#region コンストラクタ

		/// <summary>
		/// コンストラクタ
		/// </summary>
		public App() {
			// UIスレッドで実行されているコードで処理されなかったら発生する
			this.DispatcherUnhandledException += (sender, e) => {
				var ex = e.Exception;

				var sb = new StringBuilder();
				sb.AppendLine($"例外が[{ex.TargetSite.Name}]で発生しました。")
				.AppendLine("プログラムを継続しますか？");
				sb.AppendLine($"エラーメッセージ：{ex}");
				var message = sb.ToString();

				var result = MessageBox.Show(message, nameof(this.DispatcherUnhandledException), MessageBoxButton.YesNo, MessageBoxImage.Warning);
				if (result == MessageBoxResult.Yes) {
					e.Handled = true;
				}
			};
		}

		#endregion

		#region イベント

		/// <summary>
		/// アプリケーションが開始される時のイベント。
		/// </summary>
		/// <param name="e">イベント データ。</param>
		protected override void OnStartup(StartupEventArgs e) {
			DuplicateBootCheck.CheckRunning(p => {
				MessageBox.Show($"既に起動しています。\n{p}", "警告", MessageBoxButton.OK, MessageBoxImage.Stop);
				this.Shutdown();
			});

			// トップ画面の表示
			var window = new MainWindow();
			window.Show();
		}

		#endregion
	}
}
