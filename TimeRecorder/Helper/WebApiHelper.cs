﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AmsDatabaseConnectionLibrary;
using AmsDatabaseConnectionLibrary.Enums.Swan;
using AmsDatabaseConnectionLibrary.Model.Common;
using ExtensionsLibrary.Extensions;
using NetLibrary.Extensions;
using TimeRecorder.Enums;

namespace TimeRecorder.Helper {
	/// <summary>
	/// Web API を利用するためのヘルパークラスです。
	/// </summary>
	public class WebApiHelper {
		#region コンストラクタ

		/// <summary>
		/// ルートパスを指定して、
		/// WebApiHelper クラスの新しいインスタンスを初期化します。
		/// </summary>
		/// <param name="rootPath">Web API を提供するサイトのルートパス</param>
		public WebApiHelper(string rootPath) {
			this.RootPath = new Uri(rootPath);
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// Web API を提供するサイトのルートパスを取得します。
		/// </summary>
		public Uri RootPath { get; protected set; }

		#endregion

		#region メソッド

		/// <summary>
		/// [非同期]
		/// 管理者権限があるかどうかを取得します。
		/// </summary>
		/// <param name="card">カード番号</param>
		/// <returns>管理者権限がある場合は true、それ以外は false を返します。</returns>
		public async Task<bool> CheckUserHasAdministratorAuthorityAsync(string card) {
			try {
				var path = $"{this.RootPath.AbsoluteUri}/Api/Users/ByCard/{card}";
				var uri = new Uri(path);

				var user = await uri.GetJsonAsync<m_user>();
				var userKengen = user.AuthorityCode;

				return userKengen >= AuthorityCodeTypes.副店長;
			} catch (Exception) {
				return false;
			}
		}

		#region GetUsersAsync (+1 オーバーロード)

		/// <summary>
		/// [非同期]
		/// ユーザー情報のコレクションを取得します。
		/// </summary>
		/// <returns>ユーザー情報のコレクションを返します。</returns>
		public async Task<IEnumerable<m_user>> GetUsersAsync() {
			var path = $"{this.RootPath.AbsoluteUri}/Api/Users/";
			var uri = new Uri(path);

			var json = await uri.GetJsonAsync<List<m_user>>();
			return json;
		}

		/// <summary>
		/// [非同期]
		/// 所属コードを指定して、
		/// ユーザー情報のコレクションを取得します。
		/// </summary>
		/// <param name="org_id">所属コード</param>
		/// <returns>ユーザー情報のコレクションを返します。</returns>
		public async Task<IEnumerable<m_user>> GetUsersAsync(int org_id) {
			var users = await this.GetUsersAsync();
			return users.Where(u => u.org_id == org_id);
		}

		#endregion

		/// <summary>
		/// [非同期]
		/// 組織情報のコレクションを取得します。
		/// </summary>
		/// <returns>組織情報のコレクションを返します。</returns>
		public async Task<IEnumerable<m_org>> GetOrgsAsync() {
			var path = $"{this.RootPath.AbsoluteUri}/Api/Orgs/";
			var uri = new Uri(path);

			var json = await uri.GetJsonAsync<List<m_org>>();
			return json;
		}

		/// <summary>
		/// [非同期]
		/// ユーザー情報を更新
		/// </summary>
		/// <param name="id">ユーザーID</param>
		/// <param name="cardNum">カード番号</param>
		/// <returns>応答メッセージを返します。</returns>
		public async Task<HttpResponseMessage> PutCardNumAsync(string id, string cardNum) {
			var path = $"{this.RootPath.AbsoluteUri}/Api/Users/{id}";
			var uri = new Uri(path);

			return await uri.PutAsync(new m_user() {
				card_id = cardNum,
			});
		}

		/// <summary>
		/// [非同期]
		/// 勤怠種別とカード番号を指定して、
		/// 勤怠情報を登録します。
		/// </summary>
		/// <param name="workCategory">勤怠種別</param>
		/// <param name="card">カード番号</param>
		/// <returns>応答メッセージを返します。</returns>
		public async Task<HttpResponseMessage> RegistAsync(WorkCategoryTypes workCategory, string card) {
			using (var clt = new HttpClient()) {
				var path = $"{this.RootPath.AbsoluteUri}/Api/Attendances/{card}";

				switch (workCategory) {
				case WorkCategoryTypes.出勤:
					return await clt.PostAsync(path, null);
				case WorkCategoryTypes.退勤:
					return await clt.PutAsync(path, null);
				default:
					throw new ArgumentException("勤怠区分が異常です。", nameof(workCategory));
				}
			}
		}

		/// <summary>
		/// [非同期]
		/// 日次勤怠情報に出退勤情報を登録します。
		/// </summary>
		/// <param name="id">社員番号</param>
		/// <param name="entry">出勤時刻</param>
		/// <param name="exit">退勤時刻</param>
		/// <returns>応答メッセージを返します。</returns>
		public async Task<HttpResponseMessage> RegistWorkAsync(string id, DateTime entry, DateTime exit) {
			var day = entry;
			var path = $"{this.RootPath.AbsoluteUri}/Api/WorkDays/Record/{id}/{day.ToString("yyyy-MM-dd")}";
			var uri = new Uri(path);

			var workTime = exit - entry;
			var inTime = entry.TimeOfDay;
			var outTime = inTime + workTime;

			return await uri.PutAsync(new AttendanceInfo() {
				access_in_time = inTime.ToHourAndMinString(),
				access_out_time = outTime.ToHourAndMinString(),
			});
		}

		#endregion
	}
}
