﻿using System;
using TimeRecorder.Enums;
using TimeRecorder.Interface;

namespace TimeRecorder.Data {
	/// <summary>
	/// 登録データ情報
	/// </summary>
	public class RegistData : IRegistData {
		#region プロパティ

		/// <summary>
		/// 応答の成否
		/// </summary>
		public bool Success { get; set; }

		/// <summary>
		/// ユーザーID
		/// </summary>
		public string UserId { get; set; }

		/// <summary>
		/// 登録名
		/// </summary>
		public string RegisterName { get; set; }

		/// <summary>
		/// 登録時刻
		/// </summary>
		public DateTime RegisterTime {
			get {
				switch (this.WorkCategory) {
				case WorkCategoryTypes.出勤:
					return this.EntryTime;
				case WorkCategoryTypes.退勤:
					if (!this.ExitTime.HasValue) {
						throw new ArgumentException("退勤時刻が登録されていません。", nameof(this.ExitTime));
					}

					return this.ExitTime.Value;
				default:
					throw new ArgumentException("勤怠区分が異常です。", nameof(this.WorkCategory));
				}
			}
		}

		/// <summary>
		/// 出勤時刻
		/// </summary>
		public DateTime EntryTime { get; set; }

		/// <summary>
		/// 退勤時刻
		/// </summary>
		public DateTime? ExitTime { get; set; }

		/// <summary>
		/// 勤怠種別
		/// </summary>
		public WorkCategoryTypes WorkCategory { get; set; }

		#endregion

		#region メソッド

		/// <summary>
		/// 現在のオブジェクトを表す文字列に変換します。
		/// </summary>
		/// <returns>現在のオブジェクトを表す文字列を返します。</returns>
		public override string ToString()
			=> !this.Success
				? this.GetErrMessage()
				: $"{this.RegisterName}さんが{this.WorkCategory}しました。({this.RegisterTime})";

		/// <summary>
		/// エラーメッセージを取得します。
		/// </summary>
		/// <returns>エラーメッセージを返します。</returns>
		private string GetErrMessage() {
			switch (this.WorkCategory) {
			case WorkCategoryTypes.出勤:
				return $"退勤していません。";
			case WorkCategoryTypes.退勤:
				return $"出勤していません。";
			default:
				throw new ArgumentException("勤怠区分が異常です。", nameof(this.WorkCategory));
			}
		}

		#endregion
	}
}
