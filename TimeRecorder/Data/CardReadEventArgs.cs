﻿using System;

namespace TimeRecorder.Data {
	/// <summary>
	/// カードを読み込んだ際に発生するイベントのデータクラスです。
	/// </summary>
	public class CardReadEventArgs : EventArgs {
		#region コンストラクタ

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="card">カード番号</param>
		public CardReadEventArgs(string card) {
			this.CardNumber = card;
		}

		#endregion

		#region プロパティ

		/// <summary>
		/// カード番号
		/// </summary>
		public string CardNumber { get; protected set; }

		#endregion

		#region メソッド

		#endregion
	}
}
